fetch('https://newsapi.org/v2/top-headlines?country=id&apiKey=e10ef662f34d416eb904dde5c92902c3')
.then((res) => res.json())
.then((res) => {
    const news = res.articles;
    let cards = "";
    news.forEach(n => cards += showCards(n));
    const newsContainer = document.querySelector('.articel-container');
    newsContainer.innerHTML = cards;
})

const searchButton = document.querySelector('.search-button');
searchButton.addEventListener('click', function() {
    const inputKeyword = document.querySelector('.input-keyword');
    fetch('https://newsapi.org/v2/everything?apiKey=e10ef662f34d416eb904dde5c92902c3&from=2022-10-10&language=id&q=' + inputKeyword.value)
    .then((res) => res.json())
    .then((res) => {
        const news = res.articles;
        let cards = "";
        news.forEach(n => cards += showCards(n));
        const newsContainer = document.querySelector('.articel-container');
        newsContainer.innerHTML = cards;
    });
})

function showCards(a){
    return `<div class="col-md-3 my-3 ms-5 p-2">
    <div class="card">
    <img src="${a.urlToImage}" class="card-img-top" alt="">
    <div class="card-body">
        <h5 class="card-title">${a.title}</h5>
        <h6 class="card-subtitle mb-2 text-muted">${a.publishedAt}</h6>
        <a href="#" class="btn btn-primary modal-detail-button" data-bs-toggle="modal" data-bs-target="#newsDetailModal" data-imdbid="${a.articles}">Read more...</a>
    </div>
    </div>
</div>`
}